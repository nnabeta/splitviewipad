//
//  AppDelegate.h
//  SplitViewIpad
//
//  Created by 式正 鍋田 on 12/07/23.
//  Copyright (c) 2012年 Norimasa Nabeta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
